const fs = require('fs');
const xml2js  = require('xml2js');
const db = require('./db');

const files = fs.readdirSync(`${__dirname}/import`);
(async () => {
	const Preparation = [];
	Preparation.push(db.query('TRUNCATE categories;'));
	Preparation.push(db.query('TRUNCATE currencies;'));
	Preparation.push(db.query('TRUNCATE offers;'));
	await Promise.all(Preparation);

	const FilePromises = []
	files.forEach(yml => {
		FilePromises.push(new Promise(async (resolve, reject) => {
			const content = fs.readFileSync(`${__dirname}/import/${yml}`).toString();
			const context = await xml2js.parseStringPromise(content)
			const inserts = [];
			const data = context.yml_catalog.shop[0];
			data.currencies.forEach(ymlItem => {
				ymlItem.currency.forEach(currency => {
					inserts.push(db.query('INSERT INTO currencies(id, rate, plus) VALUES(?, ?, ?)', [currency.$.id, currency.$.rate, currency.$.plus]))
				})
			})
			data.categories.forEach(ymlItem => {
				ymlItem.category.forEach(category => {
					inserts.push(db.query('INSERT INTO categories(id, name) VALUES(?, ?)', [category.$.id, category._]))
				})
			})
			data.offers.forEach(ymlItem => {
				ymlItem.offer.forEach(offer => {
					inserts.push(db.query('INSERT INTO offers(id, available, categoryId, name, picture, price, currencyId, url) VALUES(?, ?, ?, ?, ?, ?, ?, ?)',
					[
						offer.$.id,
						offer.$.available ? 1 : 0,
						offer.categoryId ? offer.categoryId[0] : 0,
						offer.name ? offer.name[0] : null,
						offer.picture ? offer.picture[0] : null,
						offer.price ? offer.price[0] : null,
						offer.currencyId ? offer.currencyId[0] : null,
						offer.url ? offer.url[0].replace('__DEEPLINK-HASH__', 'qcmpea1ivbdzi31lfu2jhdiwu6anat8m') : null,
					]));
				})
			})
			await Promise.all(inserts);
			resolve();
		}));
	})
	await Promise.all(FilePromises);
	console.log('Done!')
	process.exit();
})()