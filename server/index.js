const express = require('express');
const app = express();
const http = require('http').createServer(app);
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const apiRouter = express.Router();
const db = require('./db');
const fs = require('fs');
require('./utils');

const corsOptions = {
	origin: (origin, callback) => {
		if (false) {
			callback(null, true);
		} else {
			callback(null, false);
		}
	},
	optionsSuccessStatus: 200,
	allowedHeaders: ['X-Requested-With', 'X-HTTP-Method-Override', 'Content-Type', 'Accept', 'Access-Control-Allow-Origin', 'X-Session-Key'],
	methods: ['GET', 'HEAD', 'POST', 'PUT', 'DELETE'],
	credentials: true,
};
app.options('*', cors(corsOptions));
app.use(cors(corsOptions));
app.use(bodyParser.urlencoded({
	'extended': 'true'
}));
app.use(bodyParser.json());
app.use(bodyParser.json({
	type: 'application/vnd.api+json'
}));

apiRouter.get('/categories', async (request, response) => {
	try {
		const categories = await db.query(`
			SELECT c.* FROM categories AS c WHERE EXISTS(SELECT NULL
					FROM offers AS o
				WHERE o.categoryId=c.id)
		`);
		response.json({ categories })
	} catch (error) {
		console.error(error);
		response.sendStatus(500);
	}
})
apiRouter.post('/offers', async (request, response) => {
	try {
		const {
			from,
			to,
			random,
			category,
		} = request.body;
		let offers = await db.query(`
			SELECT o.id, o.name, o.picture, o.price, o.currencyId AS currency, o.url, c.name AS category, c.image AS categoryIcon
				FROM offers AS o
				LEFT JOIN categories AS c ON o.available=1 AND c.id=o.categoryId
			${category?`WHERE o.categoryId=?`:''}
		`, category?[category]:[]);
		if (random) {
			offers = offers.shuffle().splice(0, Math.abs(to-from));
		} else {
			offers = offers.splice(from, to-from);
		}
		if (category) offers = offers.filter(offer => offer.category);
		response.json({ offers })
	} catch (error) {
		console.error(error);
		response.sendStatus(500);
	}
})

app.use('/api', apiRouter)

app.get('/', (req, res) => {
	try {
		const index = fs.readFileSync('./build/index.html', 'utf8').replace('<tag>%META%</tag>', `
			<title>Топ товаров AliExpress</title>
			<meta name="description" content="Мы собрали лучшие предложения с AliExpress для вас"/>
		`);
		res.send(index);
	} catch (error) {
		res.sendStatus(500);
	}
});

app.get('/category/:category', async (req, res) => {
	try {
		const { category } = req.params;
		const categories = await db.query(`
				SELECT * FROM categories WHERE id=?
		`, [category]);
		const index = fs.readFileSync('./build/index.html', 'utf8').replace('<tag>%META%</tag>', `
			<title>Топ товаров категории ${categories[0].name} AliExpress</title>
			<meta name="description" content="Мы собрали лучшие товары категории ${categories[0].name} с AliExpress для вас"/>
		`);
		res.send(index);
	} catch (error) {
		res.redirect('/')
	}
});

app.use(express.static(path.resolve('./build')));

http.listen(80, function () {
	console.log('listening on *:80');
});
