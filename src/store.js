import { createStore, applyMiddleware, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { sagaInit } from './Reducers/saga.js';
import storage from 'redux-persist/lib/storage';
import { persistStore, persistReducer } from 'redux-persist';

import {
  navigationReducer,
} from './Reducers/reducers.js';

const persistConfig = {
  key: 'root',
  storage,
};

const reducers = combineReducers({
  Navigation: navigationReducer,
});

const persistedReducer = persistReducer(persistConfig, reducers);

const sagaMiddleware = createSagaMiddleware();
const middlewareList = [sagaMiddleware];
const store = createStore(
  persistedReducer,
  composeWithDevTools(applyMiddleware(...middlewareList)),
);

const persistor = persistStore(store, null, () => {
  store.getState();
});

sagaMiddleware.run(sagaInit, store);

export { store, persistor };