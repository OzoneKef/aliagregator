import React from 'react';
import Preloader from '../Preloader';
import OfferList from '../OfferList';
import axios from 'axios';
import config from '../../config';
import { connect } from 'react-redux';

import './style.scss';

class CategoryList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            error: null,
            fetch: true,
			categories: [],
			category: null,
			catlimit: 0,
			width: window.innerWidth,
		}

		this.ref = React.createRef();
    }  

    componentDidMount() {
		this.recalculateCatLimit();
		
        axios.get(`${config.hostname}/api/categories`, {
			category: this.props.category,
        }).then(res => {
            this.setState({ categories: res.data.categories, fetch: false, category: res.data.categories[0].id }, () => this.recalculateCatLimit());
        }).catch(error => {
            console.error(error);
            this.setState({ error: true, fetch: false });
		});

		window.addEventListener('resize', this.recalculateCatLimit);
    }

	componentWillUnmount() {
		window.removeEventListener('resize', this.recalculateCatLimit);
	}

	recalculateCatLimit = () => {
		this.setState({ catlimit: Math.floor(this.ref.current.getBoundingClientRect().width / 140) * 4, width: window.innerWidth });
	}

	render() {
        const { categories, error, fetch, catlimit, width } = this.state;

        return <div className='CategoryList'>
			<div className='List'>
				{categories.map((category, index) => <div
					key={index}
					onClick={width > 920 ? () => this.setState({ category: category.id }) : () => this.props.navigate({ route: '/category', subroute: '/'+category.id })}
					className='categoryItem'>
					{category.name}
				</div>)}
			</div>
			<div className='categoryView' ref={this.ref} style={{ display: width > 920 ? 'block' : 'none' }}>
            	{fetch ? <Preloader/> : <>
					<OfferList key={this.state.category} category={this.state.category} to={catlimit} from={0}/>
				</>}
				<div className='ShowAllButton' onClick={() => this.props.navigate({ route: '/category', subroute: '/'+this.state.category })}>Посмотреть все</div>
			</div>
        </div>
	}
}

export default connect(state => (
    {
    }),
    {
        navigate: params => ({ type: 'NAVIGATE', params }),
    },
)(CategoryList);