import React from 'react';

import './style.scss';

export default class Offer extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			costMulitplier: 0,
		}
	}

	componentDidMount() {
		this.setState({ costMulitplier: 1.05+(Math.random()*0.2) })
	}

	render() {
		const { offer } = this.props;
        return <a className='OfferItem' href={offer.url} target="_blank">
			<label>{offer.name}</label>
			<div className='description'>
				<div className='imgBox'>
					<img src={offer.picture}/>
				</div>
				<div className='priceBox'>
					<div className='new'>
						<span className='cost'>{offer.price} </span>
						<span className='currency'>{offer.currency}</span>
					</div>
					<div className='old'>
						<span className='cost'>{(offer.price*this.state.costMulitplier).toFixed(2)} </span>
						<span className='currency'>{offer.currency}</span>
					</div>
				</div>
			</div>
        </a>
	}
}