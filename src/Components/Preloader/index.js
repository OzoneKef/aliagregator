import React from 'react';
import './style.scss';

export default class Preloader extends React.Component {
	render() {
        return <svg className="preloader" height="50" width="50"><circle className="path" cx="25" cy="25" r="20" stroke="#e72e04" fill="none" strokeWidth="2.5" strokeMiterlimit="10"></circle></svg>
	}
}