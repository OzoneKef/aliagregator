import React from 'react';
import Preloader from '../Preloader';
import Offer from '../Offer';
import axios from 'axios';
import config from '../../config';

import './style.scss';

export default class OfferList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            error: null,
            fetch: true,
            offers: [],
        }
        this.ref = document.querySelector('.routeRoot');
    }  

    componentDidMount() {
        this.assembleRequest();
        if (this.props.infinite) {
            this.ref.addEventListener('scroll', () => {
                if (!((this.ref.scrollTop + this.ref.getBoundingClientRect().height + 1) < this.ref.scrollHeight)) {
                    axios.post(`${config.hostname}/api/offers`, {
                        from: this.state.offers.length,
                        to: this.state.offers.length+20,
                        random: this.props.random,
                        category: this.props.category,
                    }).then(res => {
                        this.setState({ offers: [...this.state.offers, ...res.data.offers], fetch: false });
                    }).catch(error => {
                        console.error(error);
                        this.setState({ error: true, fetch: false });
                    });
                }
            });
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.from !== this.props.from || prevProps.to !== this.props.to || prevProps.category !== this.props.category) {
            this.assembleRequest();
        }
    }    

    assembleRequest() {
        axios.post(`${config.hostname}/api/offers`, {
            from: this.props.from || 0,
			to: this.props.to || 20,
			random: this.props.random,
			category: this.props.category,
        }).then(res => {
            this.setState({ offers: res.data.offers, fetch: false });
        }).catch(error => {
            console.error(error);
            this.setState({ error: true, fetch: false });
        });
    }

	render() {
        const { offers, error, fetch } = this.state;

        return <div className='OfferList'>
            {offers.map((offer, index) => {
                if (index < this.props.to || !this.props.to)
                return <Offer offer={offer} key={index}/>
            })}
            {fetch ? <Preloader/> : null}
        </div>
	}
}