import React from 'react';
import { connect } from 'react-redux';

import './style.scss';

class Header extends React.Component {
	render() {
        return <div className='Header'>
            <div className='Title' onClick={() => this.props.navigate({ route: '' })}>
				<h1><b>TOP</b><b className='Digit'>100</b> AliExpress</h1>
			</div>
        </div>
	}
}

export default connect(state => (
    {
    }),
    {
        navigate: params => ({ type: 'NAVIGATE', params }),
    },
)(Header);