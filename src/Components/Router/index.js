import React from 'react';
import { connect } from 'react-redux';
import { Circle } from 'react-preloaders';
import MainPage from '../../Screens/MainPage';
import Header from '../Header';
import OfferList from '../OfferList';

import './style.scss';

const routes = {
    '': props => <MainPage {...props}/>,
    '/category': props => <OfferList infinite={true} category={props.subroute.replace('/', '')}/>,
}

class Router extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            init: false,
        }
    }

    componentDidMount() {
        this.recalcPath();
        window.onpopstate = () => this.recalcPath();
        (document.querySelector('head title')||{}).innerHTML = 'Топ товаров AliExpress';
    }
    
    recalcPath = () => {
        const routeParams = window.location.pathname.split('/');
        this.props.navigate({
            route: routeParams[1]?`/${routeParams[1]}`:'',
            subroute: routeParams.length > 2 ? `/${routeParams[2]}` : null,
        })
    }

    componentDidUpdate(prevProps) {
        if (prevProps.navigation !== this.props.navigation) {
            if (!this.props.init) this.setState({ init: true });
            if (!routes[this.props.navigation.route]) this.props.navigate({
                route: '',
                subroute: null,
            });
        }
    }

	render() {
        return <>
            <Circle time={0} customLoading={!this.state.init} color={'#e72e04'}/>
            <Header/>
            <div className='routeRoot'>
                {this.state.init && routes[this.props.navigation.route] ? routes[this.props.navigation.route](this.props.navigation) : null}
            </div>
        </>
	}
}

export default connect(state => (
    {
        navigation: state.Navigation,
    }),
    {
        navigate: params => ({ type: 'NAVIGATE', params }),
    },
)(Router);