import React from 'react';
import OfferList from '../../Components/OfferList';
import CategoryList from '../../Components/CategoryList';

import './style.scss';

export default class MainPage extends React.Component {

	render() {
        return <>
			<CategoryList/>
			<div className='Delimeter'>Самые популярные товары</div>
			<OfferList infinite={true} random={true}/>
		</>
	}
}