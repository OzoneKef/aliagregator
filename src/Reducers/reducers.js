const navigationInitialState = {
	route: '/',
	subroute: null,
	params: {},
};

export const navigationReducer = (state = navigationInitialState, action) => {
    switch (action.type) {
        case 'NAVIGATE':
			window.history.pushState({}, '', `${document.location.origin}${action.params.route}${action.params.subroute?action.params.subroute:''}`)
            return {
				...state,
				route: action.params.route,
				subroute: action.params.subroute,
				params: action.params.params,
			}

        default:
            return state;
    }
}