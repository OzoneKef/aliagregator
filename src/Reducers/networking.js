import axios from 'axios';
import { store } from '../store';

const commonHeaders = {
	'Content-Type': 'application/json',
	'X-Requested-With': 'XMLHttpRequest',
};

//const getAuth = () => store.getState().User.Token;
const getAuth = () => {}

export const get = async ({ url, params }) => {
	try {
		const req = await axios.get(url, { params, headers: { ...commonHeaders, 'X-Session-Key': getAuth() || 'null' }, withCredentials: true });
		return { success: 1, data: req.data };
	} catch (error) {
		console.log('error: GET ', url, error);
		store.dispatch({ type: 'ERROR_PUSH', params: error.message });
		return { success: 0, error: error.message };
	}
};

export const post = async ({ url, data, params }) => {
	try {
		const req = await axios.post(url, data, { params, headers: { ...commonHeaders, 'X-Session-Key': getAuth() || 'null' }, withCredentials: true });
		return { success: 1, data: req.data };
	} catch (error) {
		console.log('error: POST ', url, error);
		store.dispatch({ type: 'ERROR_PUSH', params: error.message });
		return { success: 0, error: error.message };
	}
};

export const put = async ({ url, data, params }) => {
	try {
		const req = await axios.put(url, data, { params, headers: { ...commonHeaders, 'X-Session-Key': getAuth() || 'null' }, withCredentials: true });
		return { success: 1, data: req.data };
	} catch (error) {
		console.log('error: PUT ', url, error);
		store.dispatch({ type: 'ERROR_PUSH', params: error.message });
		return { success: 0, error: error.message };
	}
};

export const del = async ({ url, params }) => {
	try {
		const req = await axios.delete(url, { params, headers: { ...commonHeaders, 'X-Session-Key': getAuth() || 'null' }, withCredentials: true });
		return { success: 1, data: req.data };
	} catch (error) {
		console.log('error: DEL ', url, error);
		store.dispatch({ type: 'ERROR_PUSH', params: error.message });
		return { success: 0, error: error.message };
	}
};

export const api = {
	get,
	post,
	put,
	del,
};
