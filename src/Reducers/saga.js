//import socketIO from 'socket.io-client';
//import { eventChannel } from 'redux-saga';
/*import {
  all, put, call, takeLatest, take, spawn, takeEvery,
} from 'redux-saga/effects';*/
import {
  all,
} from 'redux-saga/effects';
//import config from '../config';
//import { store } from '../store';

//const socketUri = config.ws;
//let socket = null;

export function* sagaInit(store) {
  yield all([
    WatchActions(store),
  ]);
}

export function* WatchActions() {
  try {
    //store.dispatch({ type: 'CONNECT_SOCKET' });
    //yield takeLatest('persist/REHYDRATE', reinit);
  } catch (error) {
    console.error('WatchAction Error: ', error);
  }
}

/*
function* reinit({ payload }) {
  if (payload) yield spawn(_socketSaga, payload.Session);
}

//eslint-disable-next-line
function* connectSocket(auth) {
  socket = socketIO(socketUri + `?token=${auth}`, { secure: false, transports: ['websocket'] });
  return eventChannel((emitter) => {
    socket.on('connect', () => {
      emitter({ type: 'SOCKET_CONNECT' });
    });

    socket.on('disconnect', () => {
      emitter({ type: 'SOCKET_DISCONNECT' });
    });

    return () => { }
  });
}

export function* connect(auth) {
  yield spawn(_socketSaga, auth.params);
}

export function* _socketSaga(auth) {
  const channel = yield call(connectSocket, auth);
  while (true) {
    const channelAction = yield take(channel);
    yield put(channelAction);
  }
}
*/